"use strict";

const Express = require("express");
const Sharp = require("sharp");
const {bufferToStream, streamToBuffer} = require("./utils/utils");

const https = require("https");

let app = Express();

app.use((request, response) => {
    https.get("https://www.sunhome.ru/i/cards/39/prazdnik-den-zemli-kartinka.orig.jpg", async (res) => {
        const buffer = await streamToBuffer(res);
        const _buffer = await Sharp(buffer).resize(200).toBuffer();
        // console.log(buffer.length);
        // console.log(_buffer.length);
        response.json({ before: buffer.length, after: _buffer.length });
    });
});

if (module === require.main) {
    // [START server]
    // Start the server
    const server = app.listen(process.env.PORT || 8081, () => {
        const port = server.address().port;
        console.log(`App listening on port ${port}`);
    });
    // [END server]
}