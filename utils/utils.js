"use strict";

const {Duplex} = require("stream");

module.exports.streamToBuffer = async (stream) => {
    return new Promise((resolve, reject) => {
        let data = [];

        stream
            .on("data", chunk => {
                data.push(chunk);
            })
            .on("end", () => {
                resolve(Buffer.concat(data));
            })
            .on("error", err => {
                reject(err);
            });
    });
};

module.exports.bufferToStream = (buffer) => {
    let stream = new Duplex();
    stream.push(buffer);
    stream.push(null);
    return stream;
};